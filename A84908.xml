<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A84908">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation by his Excellency the Lord Generall, forbidding all souldiers to forbear to put their horses into the mowing-pastures.</title>
    <author>Fairfax, Thomas Fairfax, Baron, 1612-1671.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A84908 of text R211181 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.14[45]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A84908</idno>
    <idno type="STC">Wing F219</idno>
    <idno type="STC">Thomason 669.f.14[45]</idno>
    <idno type="STC">ESTC R211181</idno>
    <idno type="EEBO-CITATION">99869913</idno>
    <idno type="PROQUEST">99869913</idno>
    <idno type="VID">163033</idno>
    <idno type="PROQUESTGOID">2240926898</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A84908)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163033)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f14[45])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation by his Excellency the Lord Generall, forbidding all souldiers to forbear to put their horses into the mowing-pastures.</title>
      <author>Fairfax, Thomas Fairfax, Baron, 1612-1671.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for John Playford, and are to be sold at his shop in the Temple, or at the three Daggers in Fleet-street,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1649.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- Militia -- Early works to 1800.</term>
     <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A84908</ep:tcp>
    <ep:estc> R211181</ep:estc>
    <ep:stc> (Thomason 669.f.14[45]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>A proclamation by his Excellency the Lord Generall, forbidding all souldiers to forbear to put their horses into the mowing-pastures.</ep:title>
    <ep:author>Fairfax, Thomas Fairfax, Baron</ep:author>
    <ep:publicationYear>1649</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>277</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-09</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-11</date>
    <label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A84908-e10">
  <body xml:id="A84908-e20">
   <pb facs="tcp:163033:1" rend="simple:additions" xml:id="A84908-001-a"/>
   <div type="proclamation" xml:id="A84908-e30">
    <head xml:id="A84908-e40">
     <w lemma="a" pos="d" xml:id="A84908-001-a-0010">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A84908-001-a-0020">PROCLAMATION</w>
     <w lemma="by" pos="acp" xml:id="A84908-001-a-0030">BY</w>
     <w lemma="his" pos="po" xml:id="A84908-001-a-0040">His</w>
     <w lemma="excellency" pos="n1" xml:id="A84908-001-a-0050">Excellency</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-0060">the</w>
     <w lemma="lord" pos="n1" xml:id="A84908-001-a-0070">Lord</w>
     <w lemma="general" pos="n1" xml:id="A84908-001-a-0080">General</w>
     <pc xml:id="A84908-001-a-0090">,</pc>
     <w lemma="forbid" pos="vvg" xml:id="A84908-001-a-0100">Forbidding</w>
     <w lemma="all" pos="d" xml:id="A84908-001-a-0110">all</w>
     <w lemma="soldier" pos="n2" reg="soldiers" rend="hi" xml:id="A84908-001-a-0120">Souldiers</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-0130">to</w>
     <w lemma="forbear" pos="vvi" xml:id="A84908-001-a-0140">forbear</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-0150">to</w>
     <w lemma="put" pos="vvi" xml:id="A84908-001-a-0160">put</w>
     <w lemma="their" pos="po" xml:id="A84908-001-a-0170">their</w>
     <w lemma="horse" pos="n2" xml:id="A84908-001-a-0180">Horses</w>
     <w lemma="into" pos="acp" xml:id="A84908-001-a-0190">into</w>
     <hi xml:id="A84908-e60">
      <w lemma="mowing-pasture" pos="n2" xml:id="A84908-001-a-0200">Mowing-Pastures</w>
      <pc unit="sentence" xml:id="A84908-001-a-0210">.</pc>
     </hi>
    </head>
    <p xml:id="A84908-e70">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A84908-001-a-0220">WHereas</w>
     <w lemma="complaint" pos="n1" xml:id="A84908-001-a-0230">Complaint</w>
     <w lemma="be" pos="vvz" xml:id="A84908-001-a-0240">is</w>
     <w lemma="make" pos="vvn" xml:id="A84908-001-a-0250">made</w>
     <pc xml:id="A84908-001-a-0260">,</pc>
     <w lemma="that" pos="cs" xml:id="A84908-001-a-0270">That</w>
     <w lemma="some" pos="d" xml:id="A84908-001-a-0280">some</w>
     <w lemma="officer" pos="n2" rend="hi" xml:id="A84908-001-a-0290">Officers</w>
     <w lemma="and" pos="cc" xml:id="A84908-001-a-0300">and</w>
     <w lemma="soldier" pos="n2" reg="soldiers" rend="hi" xml:id="A84908-001-a-0310">Souldiers</w>
     <w lemma="have" pos="vvb" xml:id="A84908-001-a-0320">have</w>
     <w lemma="by" pos="acp" xml:id="A84908-001-a-0330">by</w>
     <w lemma="force" pos="n1" xml:id="A84908-001-a-0340">force</w>
     <w lemma="put" pos="vvi" xml:id="A84908-001-a-0350">put</w>
     <w lemma="their" pos="po" xml:id="A84908-001-a-0360">their</w>
     <w lemma="horse" pos="n2" xml:id="A84908-001-a-0370">Horses</w>
     <w lemma="into" pos="acp" xml:id="A84908-001-a-0380">into</w>
     <hi xml:id="A84908-e100">
      <w lemma="mowing-pasture" pos="n2" xml:id="A84908-001-a-0390">Mowing-pastures</w>
      <pc xml:id="A84908-001-a-0400">,</pc>
     </hi>
     <w lemma="justify" pos="vvg" xml:id="A84908-001-a-0410">justifying</w>
     <w lemma="themselves" pos="pr" xml:id="A84908-001-a-0420">themselves</w>
     <w lemma="therein" pos="av" xml:id="A84908-001-a-0430">therein</w>
     <w lemma="upon" pos="acp" xml:id="A84908-001-a-0440">upon</w>
     <w lemma="pretence" pos="n1" xml:id="A84908-001-a-0450">pretence</w>
     <w lemma="of" pos="acp" xml:id="A84908-001-a-0460">of</w>
     <w lemma="pay" pos="vvg" xml:id="A84908-001-a-0470">paying</w>
     <w lemma="after" pos="acp" xml:id="A84908-001-a-0480">after</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-0490">the</w>
     <w lemma="rate" pos="n1" xml:id="A84908-001-a-0500">rate</w>
     <w lemma="of" pos="acp" xml:id="A84908-001-a-0510">of</w>
     <w lemma="three" pos="crd" xml:id="A84908-001-a-0520">three</w>
     <w lemma="shilling" pos="n2" xml:id="A84908-001-a-0530">shillings</w>
     <w lemma="n/a" pos="fla" rend="hi" xml:id="A84908-001-a-0540">per</w>
     <w lemma="week" pos="n1" xml:id="A84908-001-a-0550">week</w>
     <w lemma="for" pos="acp" xml:id="A84908-001-a-0560">for</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-0570">the</w>
     <w lemma="same" pos="d" xml:id="A84908-001-a-0580">same</w>
     <pc xml:id="A84908-001-a-0590">,</pc>
     <w lemma="these" pos="d" xml:id="A84908-001-a-0600">These</w>
     <w lemma="be" pos="vvb" xml:id="A84908-001-a-0610">are</w>
     <w lemma="therefore" pos="av" xml:id="A84908-001-a-0620">therefore</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-0630">to</w>
     <w lemma="require" pos="vvi" xml:id="A84908-001-a-0640">require</w>
     <w lemma="all" pos="d" xml:id="A84908-001-a-0650">all</w>
     <w lemma="officer" pos="n2" rend="hi" xml:id="A84908-001-a-0660">Officers</w>
     <w lemma="and" pos="cc" xml:id="A84908-001-a-0670">and</w>
     <hi xml:id="A84908-e130">
      <w lemma="soldier" pos="n2" reg="soldiers" xml:id="A84908-001-a-0680">Souldiers</w>
      <pc xml:id="A84908-001-a-0690">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A84908-001-a-0700">that</w>
     <w lemma="they" pos="pns" xml:id="A84908-001-a-0710">they</w>
     <w lemma="forbear" pos="vvb" xml:id="A84908-001-a-0720">forbear</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-0730">to</w>
     <w lemma="pasture" pos="vvi" xml:id="A84908-001-a-0740">pasture</w>
     <w lemma="their" pos="po" xml:id="A84908-001-a-0750">their</w>
     <w lemma="horse" pos="n2" xml:id="A84908-001-a-0760">Horses</w>
     <w lemma="in" pos="acp" xml:id="A84908-001-a-0770">in</w>
     <w lemma="any" pos="d" xml:id="A84908-001-a-0780">any</w>
     <hi xml:id="A84908-e140">
      <w lemma="mowingground" pos="n2" xml:id="A84908-001-a-0790">Mowinggrounds</w>
      <pc xml:id="A84908-001-a-0800">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A84908-001-a-0810">And</w>
     <w lemma="if" pos="cs" xml:id="A84908-001-a-0820">if</w>
     <w lemma="any" pos="d" xml:id="A84908-001-a-0830">any</w>
     <w lemma="soldier" pos="n1" reg="soldier" rend="hi" xml:id="A84908-001-a-0840">Souldier</w>
     <w lemma="or" pos="cc" xml:id="A84908-001-a-0850">or</w>
     <w lemma="officer" pos="n1" rend="hi" xml:id="A84908-001-a-0860">Officer</w>
     <w lemma="shall" pos="vmb" xml:id="A84908-001-a-0870">shall</w>
     <w lemma="offend" pos="vvi" xml:id="A84908-001-a-0880">offend</w>
     <w lemma="herein" pos="av" xml:id="A84908-001-a-0890">herein</w>
     <pc xml:id="A84908-001-a-0900">,</pc>
     <w lemma="the" pos="d" xml:id="A84908-001-a-0910">the</w>
     <w lemma="next" pos="ord" xml:id="A84908-001-a-0920">next</w>
     <w lemma="officer" pos="n1" rend="hi" xml:id="A84908-001-a-0930">Officer</w>
     <w lemma="in" pos="acp" xml:id="A84908-001-a-0940">in</w>
     <w lemma="chief" pos="j" xml:id="A84908-001-a-0950">chief</w>
     <pc xml:id="A84908-001-a-0960">,</pc>
     <w lemma="upon" pos="acp" xml:id="A84908-001-a-0970">upon</w>
     <w lemma="notice" pos="n1" xml:id="A84908-001-a-0980">notice</w>
     <w lemma="give" pos="vvn" xml:id="A84908-001-a-0990">given</w>
     <pc xml:id="A84908-001-a-1000">,</pc>
     <w lemma="be" pos="vvz" xml:id="A84908-001-a-1010">is</w>
     <w lemma="hereby" pos="av" xml:id="A84908-001-a-1020">hereby</w>
     <w lemma="strict" pos="av-j" xml:id="A84908-001-a-1030">strictly</w>
     <w lemma="require" pos="vvn" xml:id="A84908-001-a-1040">required</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-1050">to</w>
     <w lemma="cause" pos="vvi" xml:id="A84908-001-a-1060">cause</w>
     <w lemma="double" pos="j" xml:id="A84908-001-a-1070">double</w>
     <w lemma="satisfaction" pos="n1" rend="hi" xml:id="A84908-001-a-1080">satisfaction</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-1090">to</w>
     <w lemma="be" pos="vvi" xml:id="A84908-001-a-1100">be</w>
     <w lemma="make" pos="vvn" xml:id="A84908-001-a-1110">made</w>
     <w lemma="to" pos="acp" xml:id="A84908-001-a-1120">to</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-1130">the</w>
     <w lemma="party" pos="n1" xml:id="A84908-001-a-1140">party</w>
     <w lemma="grieve" pos="vvn" xml:id="A84908-001-a-1150">grieved</w>
     <pc xml:id="A84908-001-a-1160">,</pc>
     <w lemma="as" pos="acp" xml:id="A84908-001-a-1170">as</w>
     <w lemma="he" pos="pns" xml:id="A84908-001-a-1180">he</w>
     <w lemma="will" pos="vmb" xml:id="A84908-001-a-1190">will</w>
     <w lemma="answer" pos="vvi" xml:id="A84908-001-a-1200">answer</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-1210">the</w>
     <w lemma="contrary" pos="j" xml:id="A84908-001-a-1220">contrary</w>
     <w lemma="at" pos="acp" xml:id="A84908-001-a-1230">at</w>
     <w lemma="a" pos="d" xml:id="A84908-001-a-1240">a</w>
     <hi xml:id="A84908-e190">
      <w lemma="court" pos="n1" xml:id="A84908-001-a-1250">Court</w>
      <w lemma="martial" pos="j" xml:id="A84908-001-a-1260">Martial</w>
     </hi>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-1270">to</w>
     <w lemma="be" pos="vvi" xml:id="A84908-001-a-1280">be</w>
     <w lemma="hold" pos="vvn" xml:id="A84908-001-a-1290">held</w>
     <w lemma="at" pos="acp" xml:id="A84908-001-a-1300">at</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-1310">the</w>
     <w lemma="headquarter" pos="n2" reg="headquarters" xml:id="A84908-001-a-1320">Head-quarters</w>
     <pc xml:id="A84908-001-a-1330">;</pc>
     <w lemma="the" pos="d" xml:id="A84908-001-a-1340">the</w>
     <hi xml:id="A84908-e200">
      <w lemma="judge" pos="n1" reg="judge" xml:id="A84908-001-a-1350">Judg</w>
      <w lemma="advocate" pos="n1" xml:id="A84908-001-a-1360">Advocate</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A84908-001-a-1370">of</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-1380">the</w>
     <w lemma="army" pos="n1" xml:id="A84908-001-a-1390">Army</w>
     <w lemma="be" pos="vvg" xml:id="A84908-001-a-1400">being</w>
     <w lemma="hereby" pos="av" xml:id="A84908-001-a-1410">hereby</w>
     <w lemma="require" pos="vvn" xml:id="A84908-001-a-1420">required</w>
     <pc xml:id="A84908-001-a-1430">,</pc>
     <w lemma="upon" pos="acp" xml:id="A84908-001-a-1440">upon</w>
     <w lemma="notice" pos="n1" xml:id="A84908-001-a-1450">notice</w>
     <w lemma="give" pos="vvn" xml:id="A84908-001-a-1460">given</w>
     <w lemma="of" pos="acp" xml:id="A84908-001-a-1470">of</w>
     <w lemma="such" pos="d" xml:id="A84908-001-a-1480">such</w>
     <w lemma="officer" pos="n1" rend="hi" xml:id="A84908-001-a-1490">Officer</w>
     <w lemma="his" pos="po" xml:id="A84908-001-a-1500">his</w>
     <w lemma="neglect" pos="n1" xml:id="A84908-001-a-1510">neglect</w>
     <w lemma="of" pos="acp" xml:id="A84908-001-a-1520">of</w>
     <w lemma="his" pos="po" xml:id="A84908-001-a-1530">his</w>
     <w lemma="duty" pos="n1" xml:id="A84908-001-a-1540">duty</w>
     <w lemma="herein" pos="av" xml:id="A84908-001-a-1550">herein</w>
     <pc xml:id="A84908-001-a-1560">,</pc>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-1570">to</w>
     <w lemma="call" pos="vvi" xml:id="A84908-001-a-1580">call</w>
     <w lemma="such" pos="d" xml:id="A84908-001-a-1590">such</w>
     <w lemma="officer" pos="n1" rend="hi" xml:id="A84908-001-a-1600">Officer</w>
     <w lemma="to" pos="acp" xml:id="A84908-001-a-1610">to</w>
     <w lemma="a" pos="d" xml:id="A84908-001-a-1620">an</w>
     <w lemma="account" pos="n1" xml:id="A84908-001-a-1630">Account</w>
     <w lemma="for" pos="acp" xml:id="A84908-001-a-1640">for</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-1650">the</w>
     <w lemma="same" pos="d" xml:id="A84908-001-a-1660">same</w>
     <w lemma="before" pos="acp" xml:id="A84908-001-a-1670">before</w>
     <w lemma="a" pos="d" xml:id="A84908-001-a-1680">a</w>
     <hi xml:id="A84908-e230">
      <w lemma="court" pos="n1" xml:id="A84908-001-a-1690">Court</w>
      <w lemma="martial" pos="j" xml:id="A84908-001-a-1700">Martial</w>
      <pc xml:id="A84908-001-a-1710">,</pc>
     </hi>
     <w lemma="who" pos="crq" xml:id="A84908-001-a-1720">who</w>
     <w lemma="be" pos="vvb" xml:id="A84908-001-a-1730">are</w>
     <w lemma="desire" pos="vvn" xml:id="A84908-001-a-1740">desired</w>
     <w lemma="not" pos="xx" xml:id="A84908-001-a-1750">not</w>
     <w lemma="only" pos="av-j" xml:id="A84908-001-a-1760">only</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-1770">to</w>
     <w lemma="see" pos="vvi" xml:id="A84908-001-a-1780">see</w>
     <w lemma="reparation" pos="n1" reg="reparation" rend="hi" xml:id="A84908-001-a-1790">repairation</w>
     <w lemma="make" pos="vvn" xml:id="A84908-001-a-1800">made</w>
     <w lemma="to" pos="acp" xml:id="A84908-001-a-1810">to</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-1820">the</w>
     <w lemma="party" pos="n1" rend="hi" xml:id="A84908-001-a-1830">party</w>
     <w lemma="injure" pos="vvn" xml:id="A84908-001-a-1840">injured</w>
     <pc xml:id="A84908-001-a-1850">,</pc>
     <w lemma="but" pos="acp" xml:id="A84908-001-a-1860">but</w>
     <w lemma="also" pos="av" xml:id="A84908-001-a-1870">also</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-1880">to</w>
     <w lemma="cause" pos="vvi" xml:id="A84908-001-a-1890">cause</w>
     <w lemma="good" pos="j" xml:id="A84908-001-a-1900">good</w>
     <w lemma="cost" pos="vvz" xml:id="A84908-001-a-1910">Costs</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-1920">to</w>
     <w lemma="be" pos="vvi" xml:id="A84908-001-a-1930">be</w>
     <w lemma="give" pos="vvn" xml:id="A84908-001-a-1940">given</w>
     <w lemma="he" pos="pno" xml:id="A84908-001-a-1950">him</w>
     <w lemma="for" pos="acp" xml:id="A84908-001-a-1960">for</w>
     <w lemma="his" pos="po" xml:id="A84908-001-a-1970">his</w>
     <w lemma="charge" pos="n2" xml:id="A84908-001-a-1980">Charges</w>
     <w lemma="in" pos="acp" xml:id="A84908-001-a-1990">in</w>
     <w lemma="make" pos="vvg" xml:id="A84908-001-a-2000">making</w>
     <w lemma="his" pos="po" xml:id="A84908-001-a-2010">his</w>
     <w lemma="address" pos="n1" xml:id="A84908-001-a-2020">Address</w>
     <w lemma="at" pos="acp" xml:id="A84908-001-a-2030">at</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-2040">the</w>
     <w lemma="headquarter" pos="n2" reg="headquarters" xml:id="A84908-001-a-2050">Head-quarters</w>
     <pc unit="sentence" xml:id="A84908-001-a-2060">.</pc>
    </p>
    <closer xml:id="A84908-e260">
     <dateline xml:id="A84908-e270">
      <w lemma="give" pos="vvn" xml:id="A84908-001-a-2070">Given</w>
      <w lemma="under" pos="acp" xml:id="A84908-001-a-2080">under</w>
      <w lemma="my" pos="po" xml:id="A84908-001-a-2090">my</w>
      <w lemma="hand" pos="n1" xml:id="A84908-001-a-2100">hand</w>
      <w lemma="and" pos="cc" xml:id="A84908-001-a-2110">and</w>
      <w lemma="seal" pos="vvi" xml:id="A84908-001-a-2120">seal</w>
      <date xml:id="A84908-e280">
       <w lemma="the" pos="d" xml:id="A84908-001-a-2130">the</w>
       <w lemma="21." pos="crd" xml:id="A84908-001-a-2140">21.</w>
       <w lemma="of" pos="acp" xml:id="A84908-001-a-2150">of</w>
       <w lemma="June" pos="nn1" xml:id="A84908-001-a-2160">June</w>
       <pc xml:id="A84908-001-a-2170">,</pc>
       <w lemma="1649." pos="crd" xml:id="A84908-001-a-2180">1649.</w>
       <pc unit="sentence" xml:id="A84908-001-a-2190"/>
      </date>
     </dateline>
     <signed xml:id="A84908-e290">
      <w lemma="t." pos="ab" xml:id="A84908-001-a-2200">T.</w>
      <w lemma="FAIRFAX" pos="nn1" xml:id="A84908-001-a-2210">FAIRFAX</w>
      <pc unit="sentence" xml:id="A84908-001-a-2220">.</pc>
     </signed>
    </closer>
   </div>
   <div type="license" xml:id="A84908-e300">
    <p xml:id="A84908-e310">
     <w lemma="this" pos="d" xml:id="A84908-001-a-2230">This</w>
     <w lemma="proclamation" pos="n1" xml:id="A84908-001-a-2240">Proclamation</w>
     <w lemma="be" pos="vvz" xml:id="A84908-001-a-2250">is</w>
     <w lemma="forthwith" pos="av" xml:id="A84908-001-a-2260">forthwith</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-2270">to</w>
     <w lemma="be" pos="vvi" xml:id="A84908-001-a-2280">be</w>
     <w lemma="print" pos="vvn" xml:id="A84908-001-a-2290">printed</w>
     <w lemma="and" pos="cc" xml:id="A84908-001-a-2300">and</w>
     <w lemma="send" pos="vvn" xml:id="A84908-001-a-2310">sent</w>
     <w lemma="to" pos="acp" xml:id="A84908-001-a-2320">to</w>
     <w lemma="every" pos="d" xml:id="A84908-001-a-2330">every</w>
     <w lemma="regiment" pos="n1" xml:id="A84908-001-a-2340">Regiment</w>
     <w lemma="of" pos="acp" xml:id="A84908-001-a-2350">of</w>
     <w lemma="horse" pos="n1" xml:id="A84908-001-a-2360">Horse</w>
     <pc xml:id="A84908-001-a-2370">,</pc>
     <w lemma="and" pos="cc" xml:id="A84908-001-a-2380">and</w>
     <w lemma="proclaim" pos="vvn" xml:id="A84908-001-a-2390">proclaimed</w>
     <w lemma="at" pos="acp" xml:id="A84908-001-a-2400">at</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-2410">the</w>
     <w lemma="head" pos="n1" xml:id="A84908-001-a-2420">Head</w>
     <w lemma="of" pos="acp" xml:id="A84908-001-a-2430">of</w>
     <w lemma="each" pos="d" xml:id="A84908-001-a-2440">each</w>
     <w lemma="regiment" pos="n1" xml:id="A84908-001-a-2450">Regiment</w>
     <w lemma="or" pos="cc" xml:id="A84908-001-a-2460">or</w>
     <w lemma="troop" pos="n1" xml:id="A84908-001-a-2470">Troop</w>
     <pc unit="sentence" xml:id="A84908-001-a-2480">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A84908-e320">
   <div type="colophon" xml:id="A84908-e330">
    <p xml:id="A84908-e340">
     <hi xml:id="A84908-e350">
      <w lemma="LONDON" pos="nn1" xml:id="A84908-001-a-2490">LONDON</w>
      <pc xml:id="A84908-001-a-2500">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A84908-001-a-2510">Printed</w>
     <w lemma="for" pos="acp" xml:id="A84908-001-a-2520">for</w>
     <hi xml:id="A84908-e360">
      <w lemma="John" pos="nn1" xml:id="A84908-001-a-2530">John</w>
      <w lemma="Playford" pos="nn1" xml:id="A84908-001-a-2540">Playford</w>
      <pc xml:id="A84908-001-a-2550">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A84908-001-a-2560">and</w>
     <w lemma="be" pos="vvb" xml:id="A84908-001-a-2570">are</w>
     <w lemma="to" pos="prt" xml:id="A84908-001-a-2580">to</w>
     <w lemma="be" pos="vvi" xml:id="A84908-001-a-2590">be</w>
     <w lemma="sell" pos="vvn" xml:id="A84908-001-a-2600">sold</w>
     <w lemma="at" pos="acp" xml:id="A84908-001-a-2610">at</w>
     <w lemma="his" pos="po" xml:id="A84908-001-a-2620">his</w>
     <w lemma="shop" pos="n1" xml:id="A84908-001-a-2630">shop</w>
     <w lemma="in" pos="acp" xml:id="A84908-001-a-2640">in</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-2650">the</w>
     <hi xml:id="A84908-e370">
      <w lemma="temple" pos="n1" xml:id="A84908-001-a-2660">Temple</w>
      <pc xml:id="A84908-001-a-2670">,</pc>
     </hi>
     <w lemma="or" pos="cc" xml:id="A84908-001-a-2680">or</w>
     <w lemma="at" pos="acp" xml:id="A84908-001-a-2690">at</w>
     <w lemma="the" pos="d" xml:id="A84908-001-a-2700">the</w>
     <w lemma="three" pos="crd" xml:id="A84908-001-a-2710">three</w>
     <w lemma="dagger" pos="n2" xml:id="A84908-001-a-2720">Daggers</w>
     <w lemma="in" pos="acp" xml:id="A84908-001-a-2730">in</w>
     <hi xml:id="A84908-e380">
      <w lemma="Fleetstreet" pos="nn1" reg="Fleetstreet" xml:id="A84908-001-a-2740">Fleet-street</w>
      <pc xml:id="A84908-001-a-2750">,</pc>
     </hi>
     <w lemma="1649." pos="crd" xml:id="A84908-001-a-2760">1649.</w>
     <pc unit="sentence" xml:id="A84908-001-a-2770"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
