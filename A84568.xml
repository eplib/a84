<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A84568">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>An act declaring and constituting the people of England to be a commonwealth and free-state.</title>
    <title>Laws, etc.</title>
    <author>England and Wales.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A84568 of text R25373 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason E1060_29). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A84568</idno>
    <idno type="STC">Wing E986</idno>
    <idno type="STC">Thomason E1060_29</idno>
    <idno type="STC">ESTC R25373</idno>
    <idno type="EEBO-CITATION">99872092</idno>
    <idno type="PROQUEST">99872092</idno>
    <idno type="VID">169029</idno>
    <idno type="PROQUESTGOID">2264190419</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A84568)</note>
    <note>Transcribed from: (Early English Books Online ; image set 169029)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 157:E1060[29])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>An act declaring and constituting the people of England to be a commonwealth and free-state.</title>
      <title>Laws, etc.</title>
      <author>England and Wales.</author>
     </titleStmt>
     <extent>1 sheet ([2] p.)</extent>
     <publicationStmt>
      <publisher>printed for Edward Husband, printer to the Honorable House of Commons, and are to be sold at his shop in Fleetstreet, at the sign of the Golden-Dragon, near the Inner-Temple,</publisher>
      <pubPlace>London :</pubPlace>
      <date>May 21. 1649.</date>
     </publicationStmt>
     <notesStmt>
      <note>Steele notation: and be or.</note>
      <note>Also issued as part of a through-paged folio set with a table for the entire set added and as part of "An act prohibiting the proclaiming of any person to be King" London, printed by John Field for Edward Husband, 1649.</note>
      <note>Ornament at head; initial letter; text in black letter.</note>
      <note>Order to print dated 19 May 1649.</note>
      <note>Reproductions of the originals in the British Library (Thomason Tracts) and the Harvard University Library (Early English books).</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
     <term>Great Britain -- Constitutional history -- 17th century -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A84568</ep:tcp>
    <ep:estc> R25373</ep:estc>
    <ep:stc> (Thomason E1060_29). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>An act declaring and constituting the people of England to be a commonwealth and free-state.:</ep:title>
    <ep:author>England and Wales. </ep:author>
    <ep:publicationYear>1649</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>207</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-09</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-11</date>
    <label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A84568-e10">
  <body xml:id="A84568-e20">
   <pb facs="tcp:169029:1" rend="simple:additions" xml:id="A84568-001-a"/>
   <div type="Act_of_Parliament" xml:id="A84568-e30">
    <head xml:id="A84568-e40">
     <figure xml:id="A84568-e50"/>
     <lb xml:id="A84568-e60"/>
     <w lemma="a" pos="d" xml:id="A84568-001-a-0010">An</w>
     <w lemma="act" pos="n1" xml:id="A84568-001-a-0020">Act</w>
     <w lemma="declare" pos="vvg" xml:id="A84568-001-a-0030">Declaring</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0040">and</w>
     <w lemma="constitute" pos="vvg" xml:id="A84568-001-a-0050">Constituting</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0060">the</w>
     <w lemma="people" pos="n1" xml:id="A84568-001-a-0070">People</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-0080">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A84568-001-a-0090">England</w>
     <w lemma="to" pos="prt" xml:id="A84568-001-a-0100">to</w>
     <w lemma="be" pos="vvi" xml:id="A84568-001-a-0110">be</w>
     <w lemma="a" pos="d" xml:id="A84568-001-a-0120">a</w>
     <w lemma="commonwealth" pos="n1" xml:id="A84568-001-a-0130">Commonwealth</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0140">and</w>
     <w lemma="free-state" pos="n1" xml:id="A84568-001-a-0150">Free-State</w>
     <pc unit="sentence" xml:id="A84568-001-a-0160">.</pc>
    </head>
    <p xml:id="A84568-e80">
     <w lemma="be" pos="vvb" rend="decorinit" xml:id="A84568-001-a-0170">BE</w>
     <w lemma="it" pos="pn" xml:id="A84568-001-a-0180">it</w>
     <w lemma="declare" pos="vvn" xml:id="A84568-001-a-0190">Declared</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0200">and</w>
     <w lemma="enact" pos="vvn" xml:id="A84568-001-a-0210">Enacted</w>
     <w lemma="by" pos="acp" xml:id="A84568-001-a-0220">by</w>
     <w lemma="this" pos="d" xml:id="A84568-001-a-0230">this</w>
     <w lemma="present" pos="j" xml:id="A84568-001-a-0240">present</w>
     <hi xml:id="A84568-e90">
      <w lemma="parliament" pos="n1" xml:id="A84568-001-a-0250">Parliament</w>
      <pc xml:id="A84568-001-a-0260">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0270">and</w>
     <w lemma="by" pos="acp" xml:id="A84568-001-a-0280">by</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0290">the</w>
     <w lemma="authority" pos="n1" xml:id="A84568-001-a-0300">authority</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-0310">of</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0320">the</w>
     <w lemma="same" pos="d" xml:id="A84568-001-a-0330">same</w>
     <pc xml:id="A84568-001-a-0340">,</pc>
     <w lemma="that" pos="cs" xml:id="A84568-001-a-0350">That</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0360">the</w>
     <w lemma="people" pos="n1" xml:id="A84568-001-a-0370">People</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-0380">of</w>
     <hi xml:id="A84568-e100">
      <w lemma="England" pos="nn1" xml:id="A84568-001-a-0390">England</w>
      <pc xml:id="A84568-001-a-0400">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0410">and</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-0420">of</w>
     <w lemma="all" pos="d" xml:id="A84568-001-a-0430">all</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0440">the</w>
     <w lemma="dominion" pos="n2" xml:id="A84568-001-a-0450">Dominions</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0460">and</w>
     <w lemma="territory" pos="n2" xml:id="A84568-001-a-0470">Territories</w>
     <w lemma="thereunto" pos="av" xml:id="A84568-001-a-0480">thereunto</w>
     <w lemma="belong" pos="vvg" xml:id="A84568-001-a-0490">belonging</w>
     <pc xml:id="A84568-001-a-0500">,</pc>
     <w lemma="be" pos="vvb" xml:id="A84568-001-a-0510">are</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0520">and</w>
     <w lemma="shall" pos="vmb" xml:id="A84568-001-a-0530">shall</w>
     <w lemma="be" pos="vvi" xml:id="A84568-001-a-0540">be</w>
     <pc xml:id="A84568-001-a-0550">,</pc>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0560">and</w>
     <w lemma="be" pos="vvb" xml:id="A84568-001-a-0570">are</w>
     <w lemma="hereby" pos="av" xml:id="A84568-001-a-0580">hereby</w>
     <w lemma="constitute" pos="vvn" xml:id="A84568-001-a-0590">Constituted</w>
     <pc xml:id="A84568-001-a-0600">,</pc>
     <w lemma="make" pos="vvn" xml:id="A84568-001-a-0610">Made</w>
     <pc xml:id="A84568-001-a-0620">,</pc>
     <w lemma="establish" pos="vvn" xml:id="A84568-001-a-0630">Established</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0640">and</w>
     <w lemma="confirm" pos="vvn" xml:id="A84568-001-a-0650">Confirmed</w>
     <pc xml:id="A84568-001-a-0660">,</pc>
     <w lemma="to" pos="prt" xml:id="A84568-001-a-0670">to</w>
     <w lemma="be" pos="vvi" xml:id="A84568-001-a-0680">be</w>
     <w lemma="a" pos="d" xml:id="A84568-001-a-0690">a</w>
     <w lemma="commonwealth" pos="n1" reg="commonwealth" xml:id="A84568-001-a-0700">Common-wealth</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0710">and</w>
     <w lemma="free-state" pos="n1" xml:id="A84568-001-a-0720">Free-State</w>
     <pc xml:id="A84568-001-a-0730">:</pc>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0740">And</w>
     <w lemma="shall" pos="vmb" xml:id="A84568-001-a-0750">shall</w>
     <w lemma="from" pos="acp" xml:id="A84568-001-a-0760">from</w>
     <w lemma="henceforth" pos="av" xml:id="A84568-001-a-0770">henceforth</w>
     <w lemma="be" pos="vvi" xml:id="A84568-001-a-0780">be</w>
     <w lemma="govern" pos="vvn" xml:id="A84568-001-a-0790">Governed</w>
     <w lemma="as" pos="acp" xml:id="A84568-001-a-0800">as</w>
     <w lemma="a" pos="d" xml:id="A84568-001-a-0810">a</w>
     <w lemma="commonwealth" pos="n1" xml:id="A84568-001-a-0820">Commonwealth</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-0830">and</w>
     <w lemma="free-state" pos="n1" xml:id="A84568-001-a-0840">Free-State</w>
     <pc xml:id="A84568-001-a-0850">,</pc>
     <w lemma="by" pos="acp" xml:id="A84568-001-a-0860">by</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0870">the</w>
     <w lemma="supreme" pos="j" xml:id="A84568-001-a-0880">Supreme</w>
     <w lemma="authority" pos="n1" xml:id="A84568-001-a-0890">Authority</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-0900">of</w>
     <w lemma="this" pos="d" xml:id="A84568-001-a-0910">this</w>
     <w lemma="nation" pos="n1" xml:id="A84568-001-a-0920">Nation</w>
     <pc xml:id="A84568-001-a-0930">,</pc>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0940">The</w>
     <w lemma="representative" pos="n2" xml:id="A84568-001-a-0950">Representatives</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-0960">of</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-0970">the</w>
     <w lemma="people" pos="n1" xml:id="A84568-001-a-0980">People</w>
     <w lemma="in" pos="acp" xml:id="A84568-001-a-0990">in</w>
     <w lemma="parliament" pos="n1" xml:id="A84568-001-a-1000">Parliament</w>
     <pc xml:id="A84568-001-a-1010">,</pc>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-1020">and</w>
     <w lemma="by" pos="acp" xml:id="A84568-001-a-1030">by</w>
     <w lemma="such" pos="d" xml:id="A84568-001-a-1040">such</w>
     <w lemma="as" pos="acp" xml:id="A84568-001-a-1050">as</w>
     <w lemma="they" pos="pns" xml:id="A84568-001-a-1060">they</w>
     <w lemma="shall" pos="vmb" xml:id="A84568-001-a-1070">shall</w>
     <w lemma="appoint" pos="vvi" xml:id="A84568-001-a-1080">appoint</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-1090">and</w>
     <w lemma="constitute" pos="vvi" xml:id="A84568-001-a-1100">constitute</w>
     <w lemma="as" pos="acp" xml:id="A84568-001-a-1110">as</w>
     <w lemma="officer" pos="n2" xml:id="A84568-001-a-1120">Officers</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-1130">and</w>
     <w lemma="minister" pos="n2" xml:id="A84568-001-a-1140">Ministers</w>
     <w lemma="under" pos="acp" xml:id="A84568-001-a-1150">under</w>
     <w lemma="they" pos="pno" xml:id="A84568-001-a-1160">them</w>
     <w lemma="for" pos="acp" xml:id="A84568-001-a-1170">for</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-1180">the</w>
     <w lemma="good" pos="j" xml:id="A84568-001-a-1190">good</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-1200">of</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-1210">the</w>
     <w lemma="people" pos="n1" xml:id="A84568-001-a-1220">People</w>
     <pc xml:id="A84568-001-a-1230">,</pc>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-1240">and</w>
     <w lemma="that" pos="cs" xml:id="A84568-001-a-1250">that</w>
     <w lemma="without" pos="acp" xml:id="A84568-001-a-1260">without</w>
     <w lemma="any" pos="d" xml:id="A84568-001-a-1270">any</w>
     <w lemma="king" pos="n1" xml:id="A84568-001-a-1280">King</w>
     <w lemma="or" pos="cc" xml:id="A84568-001-a-1290">or</w>
     <w lemma="house" pos="n1" xml:id="A84568-001-a-1300">House</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-1310">of</w>
     <w lemma="lord" pos="n2" xml:id="A84568-001-a-1320">Lords</w>
     <pc unit="sentence" xml:id="A84568-001-a-1330">.</pc>
    </p>
   </div>
   <div type="license" xml:id="A84568-e110">
    <opener xml:id="A84568-e120">
     <dateline xml:id="A84568-e130">
      <date xml:id="A84568-e140">
       <w lemma="n/a" pos="fla" xml:id="A84568-001-a-1340">Die</w>
       <w lemma="n/a" pos="fla" xml:id="A84568-001-a-1350">Sabbathi</w>
       <pc xml:id="A84568-001-a-1360">,</pc>
       <w lemma="19" pos="crd" xml:id="A84568-001-a-1370">19</w>
       <w lemma="n/a" pos="fla" xml:id="A84568-001-a-1380">Maii</w>
       <pc xml:id="A84568-001-a-1390">,</pc>
       <w lemma="1649." pos="crd" xml:id="A84568-001-a-1400">1649.</w>
       <pc unit="sentence" xml:id="A84568-001-a-1410"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="A84568-e150">
     <w lemma="order" pos="j-vn" xml:id="A84568-001-a-1420">ORdered</w>
     <w lemma="by" pos="acp" xml:id="A84568-001-a-1430">by</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-1440">the</w>
     <w lemma="commons" pos="n2" xml:id="A84568-001-a-1450">Commons</w>
     <w lemma="assemble" pos="vvn" xml:id="A84568-001-a-1460">assembled</w>
     <w lemma="in" pos="acp" xml:id="A84568-001-a-1470">in</w>
     <w lemma="parliament" pos="n1" xml:id="A84568-001-a-1480">Parliament</w>
     <pc xml:id="A84568-001-a-1490">,</pc>
     <w lemma="that" pos="cs" xml:id="A84568-001-a-1500">That</w>
     <w lemma="this" pos="d" xml:id="A84568-001-a-1510">this</w>
     <w lemma="act" pos="n1" xml:id="A84568-001-a-1520">Act</w>
     <w lemma="be" pos="vvb" xml:id="A84568-001-a-1530">be</w>
     <w lemma="forthwith" pos="av" xml:id="A84568-001-a-1540">forthwith</w>
     <w lemma="print" pos="vvn" xml:id="A84568-001-a-1550">Printed</w>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-1560">and</w>
     <w lemma="publish" pos="vvn" xml:id="A84568-001-a-1570">Published</w>
     <pc unit="sentence" xml:id="A84568-001-a-1580">.</pc>
    </p>
    <closer xml:id="A84568-e160">
     <signed xml:id="A84568-e170">
      <w lemma="hen" pos="ab" xml:id="A84568-001-a-1590">Hen:</w>
      <w lemma="Scobell" pos="nn1" xml:id="A84568-001-a-1610">Scobell</w>
      <pc xml:id="A84568-001-a-1620">,</pc>
     </signed>
     <w lemma="cleric" pos="n1" xml:id="A84568-001-a-1630">Cleric</w>
     <pc unit="sentence" xml:id="A84568-001-a-1640">.</pc>
     <w lemma="n/a" pos="fla" xml:id="A84568-001-a-1650">Parliamenti</w>
     <pc unit="sentence" xml:id="A84568-001-a-1660">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A84568-e180">
   <div type="colophon" xml:id="A84568-e190">
    <p xml:id="A84568-e200">
     <hi xml:id="A84568-e210">
      <w lemma="london" pos="nn1" xml:id="A84568-001-a-1670">London</w>
      <pc xml:id="A84568-001-a-1680">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A84568-001-a-1690">Printed</w>
     <w lemma="for" pos="acp" xml:id="A84568-001-a-1700">for</w>
     <hi xml:id="A84568-e220">
      <w lemma="Edward" pos="nn1" xml:id="A84568-001-a-1710">Edward</w>
      <w lemma="husband" pos="n1" xml:id="A84568-001-a-1720">Husband</w>
      <pc xml:id="A84568-001-a-1730">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A84568-001-a-1740">Printer</w>
     <w lemma="to" pos="acp" xml:id="A84568-001-a-1750">to</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-1760">the</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A84568-001-a-1770">Honorable</w>
     <w lemma="house" pos="n1" xml:id="A84568-001-a-1780">House</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-1790">of</w>
     <w lemma="commons" pos="n2" xml:id="A84568-001-a-1800">Commons</w>
     <pc xml:id="A84568-001-a-1810">,</pc>
     <w lemma="and" pos="cc" xml:id="A84568-001-a-1820">and</w>
     <w lemma="be" pos="vvb" xml:id="A84568-001-a-1830">are</w>
     <w lemma="to" pos="prt" xml:id="A84568-001-a-1840">to</w>
     <w lemma="be" pos="vvi" xml:id="A84568-001-a-1850">be</w>
     <w lemma="sell" pos="vvn" xml:id="A84568-001-a-1860">sold</w>
     <w lemma="at" pos="acp" xml:id="A84568-001-a-1870">at</w>
     <w lemma="his" pos="po" xml:id="A84568-001-a-1880">his</w>
     <w lemma="ship" pos="n1" xml:id="A84568-001-a-1890">Ship</w>
     <w lemma="in" pos="acp" xml:id="A84568-001-a-1900">in</w>
     <w lemma="Fleetstreet" pos="nn1" xml:id="A84568-001-a-1910">Fleetstreet</w>
     <pc xml:id="A84568-001-a-1920">,</pc>
     <w lemma="at" pos="acp" xml:id="A84568-001-a-1930">at</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-1940">the</w>
     <w lemma="sign" pos="n1" xml:id="A84568-001-a-1950">Sign</w>
     <w lemma="of" pos="acp" xml:id="A84568-001-a-1960">of</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-1970">the</w>
     <w lemma="golden-dragon" pos="n1" xml:id="A84568-001-a-1980">Golden-Dragon</w>
     <pc xml:id="A84568-001-a-1990">,</pc>
     <w lemma="near" pos="acp" xml:id="A84568-001-a-2000">near</w>
     <w lemma="the" pos="d" xml:id="A84568-001-a-2010">the</w>
     <w lemma="inner-temple" pos="nn1" xml:id="A84568-001-a-2020">Inner-Temple</w>
     <pc xml:id="A84568-001-a-2030">,</pc>
     <w lemma="may" pos="nn1" rend="hi" xml:id="A84568-001-a-2040">May</w>
     <w lemma="21." pos="crd" xml:id="A84568-001-a-2050">21.</w>
     <w lemma="1649." pos="crd" xml:id="A84568-001-a-2060">1649.</w>
     <pc unit="sentence" xml:id="A84568-001-a-2070"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
